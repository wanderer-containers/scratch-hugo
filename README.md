# [scratch-hugo](https://git.dotya.ml/wanderer-containers/scratch-hugo)

[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/scratch-hugo/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer-containers/scratch-hugo)
[![Docker Image Version](https://img.shields.io/docker/v/immawanderer/scratch-hugo/linux-amd64)](https://hub.docker.com/r/immawanderer/scratch-hugo/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/immawanderer/scratch-hugo/linux-amd64)](https://hub.docker.com/r/immawanderer/scratch-hugo/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker pulls](https://img.shields.io/docker/pulls/immawanderer/scratch-hugo)](https://hub.docker.com/r/immawanderer/scratch-hugo/)

This repository provides a Containerfile to effortlessly get a container image containing statically linked [Hugo](https://gohugo.io) static site generator (extended version) and absolutely nothing else.

The image is rebuilt weekly in CI and automatically pushed to [DockerHub](https://hub.docker.com/r/immawanderer/scratch-hugo).

The project lives at [this Gitea instance](https://git.dotya.ml/wanderer-containers/scratch-hugo).

### What you get
* `hugo-<x>.<y>.<z>+extended` (latest stable version listed on [GitHub](https://github.com/gohugoio/hugo/releases)), freshly built from sources.

### Purpose
* container image containing `Hugo` extended version, suitable for CI.

### License
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
