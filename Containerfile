# syntax=docker/dockerfile-upstream:master-labs
FROM docker.io/library/golang:1.18.5-alpine3.16 AS hugobuild

ENV HUGO_VERSION 0.102.1

# this could be so much simpler..
# ref: https://docs.docker.com/engine/reference/builder/#adding-a-git-repository-add-git-ref-dir
# ADD --keep-git-dir=true https://github.com/gohugoio/hugo.git#v${HUGO_VERSION} /hugo

RUN apk add --no-cache git musl-dev gcc g++ && \
    git clone https://github.com/gohugoio/hugo.git /hugo

WORKDIR /hugo

RUN git checkout "v${HUGO_VERSION}" && \
    \
    CGO_ENABLED=1 GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw" \
    go build -v -tags extended -ldflags '-s -w -extldflags "-static-pie"' \
    && ./hugo version

FROM scratch

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image with nothing but Hugo static site generator."
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/scratch-hugo.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0-or-later

COPY --from=hugobuild /hugo/hugo /hugo
CMD ["/hugo"]

#  vim: set ts=4 ft=dockerfile fenc=utf-8 ff=unix :
